# clo-wp-plugin-visualization

The Visualization Plugin provides a means for displaying charts and graphs of data on Word Press pages using a custom post type. 



## The Custom Post Type
When the visualization plugin is enabled, a new category on the left navigation sidebar called ***Visualization*** will appear. Click this link to add a new visualization post or update an existing one. 

A post created within the **Visualization** area has the following options: 

* **Media**: You will use this box to upload the files that the post will use to generate the graphics; it functions exactly like the **Media** box on any Word Press post, but the data must conform to the format that is specified in the File Formats section below;
* **Explanatory Text**: The text that you enter in this field will display on the visualization post underneath the chart; 
* **Data Select Label**: The text you enter into this field will appear as the heading just above the selection control ("e.g., "Select the source species"); since Line charts do not have a selection control, this value does not apply to them; 
* **Visualization Type**: This field determines the type of the visualization as follows: 
    * **Vertical Bar**: In this type of chart, the data are presented as bars that are vertical, the groups being listed along the horizontal axis; 
    * **Horizontal Bar**: In this type of chart, the data are presented as bars that are horizontal, the groups being listed along the vertical axis;
    * **Pie**: The pie chart displays one group at a time, with the pie chart segments corresponding to the group's member data; the selector control allows the user to determine which group is displayed;  
    * **Line**: The line chart displays all the data points in a dataset as a line chart.
* **Bar Style**: This field determines the arrangement of member data within a bar:
    * **Stacked**: With this style, all of the groups are displayed with their member data represented by colored segments within each bar;
    * **Grouped**: In the grouped style, the member data are shown in separate bars side-by-side;

  In both types, each member is represented by its own color;
* **Selection Type**: This field determines whether entire groups, or members within groups, are affected by the selection control:
    * **By Group**: The items listed in the selection control are groups, and checking or unchecking an item removes the group from the chart; 
    * **By Member**: The items listed in the selection control are members from all groups (these should be the same as the items listed in the legend): checking or unchecking an item results in removing the member data across all groups in which the member appears;
* **X Axis Label**, **Y Axis Label**: These fields they are used to create the labels for the X and Y axes of Line Charts and Bar Charts;
* **Visualization Taxonomy**: The taxonomy tag indicates the study to which a visualization post belongs; all pages sharing the same Visualization Taxnomy tag will be displayed in a list in the upper right corner of the post; there must be exactly one Visualization Taxonomy tag per visualization post. 
     

## File Formats
The structure of the data attached as media to the post must conform precisely to the formats specified here. 

NOTE: In all formats, it is assumed that the first row contains described meta data that are ignored by the algorithm. If the first row of a spreadsheet is empty, and you export it to CSV, the first data row will end up as the first CSV row, and it will be ignored by the algorithm. 

### Grouped 
The grouped file format is used for both stacked bar charts and pie charts. The following two examples demonstrate how such data are to be arranged in a spreadsheet-like format. **_To be consumed by the application, they must be in CSV format_**. Typically, you would edit the data in Excel or a similar program and then export it to CSV.  


This table structure illustrates the generic format. Each column represents a **group**. Each group is comprised of **members**, which are the rows under the group. For example, the series in the row under **group 3** is 4, 0, 84, and 10, which comprise the member values of the group. The names in the first column for the corresponding rows indicate the member names.   

The first row is meta data that you can use for any purpose. Typically, it is helpful to identify the kind of data that the members and groups correspond to. It is ignored by the code, but there must be some text in this row.

```
<member-row-names>:	<group-cols-names>			
	                group 1	            group 2	    group 3	            group 4 
member 1        	58	                21	        4	                1
member 2            0	                0	        0	                0
member 3        	0	                4	        84	                10
member 4    	    0	                1	        10	                29
```

The JSON representation of these data is as follows: 

```
{
   "dataset_values":[
      {
         "group_name":"group 1",
         "group_values":[
            {
               "member_name":"member 1",
               "member_value":"58"
            },
            {
               "member_name":"member 2",
               "member_value":"0"
            },
            {
               "member_name":"member 3",
               "member_value":"0"
            },
            {
               "member_name":"member 4",
               "member_value":"0"
            }
         ]
      },
      {
         "group_name":"group 2",
         "group_values":[
            {
               "member_name":"member 1",
               "member_value":"21"
            },
            {
               "member_name":"member 2",
               "member_value":"0"
            },
            {
               "member_name":"member 3",
               "member_value":"4"
            },
            {
               "member_name":"member 4",
               "member_value":"1"
            }
         ]
      },
      {
         "group_name":"group 3",
         "group_values":[
            {
               "member_name":"member 1",
               "member_value":"4"
            },
            {
               "member_name":"member 2",
               "member_value":"0"
            },
            {
               "member_name":"member 3",
               "member_value":"84"
            },
            {
               "member_name":"member 4",
               "member_value":"10"
            }
         ]
      },
      {
         "group_name":"group 4",
         "group_values":[
            {
               "member_name":"member 1",
               "member_value":"1"
            },
            {
               "member_name":"member 2",
               "member_value":"0"
            },
            {
               "member_name":"member 3",
               "member_value":"10"
            },
            {
               "member_name":"member 4",
               "member_value":"29"
            }
         ]
      }
   ]
}
```
	                
When these data are represented in a visualization, they will appear as follows: 

* **Bar Chart**:
    * Each group corresponds to a bar or set of bars, with the name of the bar as the value of the column heading for the group (in the jSON, the group_name key's value);
    * Each member of the group will be a segment any bar, with its own color labeled according to the member row's column (in the JSON, the member_name key's value); 
* **Pie Chart**: 
    * Each group represents the data for an entire pie chart; 
    * The member data correspond to the slices of the pie.

Here is an example of some grouped data for an actual study:

```
Displaced Species:	Displacing Species			
	                American Goldfinch	Bluejay  European Starling
American Goldfinch	58	                21	     4
Bluejay	            0	                0	     0
European Starling	0	                4	     84
Mourning Dove	    0	                1	     10
```

### XY Format
The XY format represents the data points for a line chart. It too must be in CSV format. Here is how it looks as a table: 

```
time	total_vocalizations
08:07	1
08:08	6
08:09	6
08:10	7
08:11	10
08:12	8
08:13	12
08:14	6
08:15	6
08:16	6
08:17	5
07:54	5
07:55	6
07:56	6
07:57	6
07:58	6
07:59	4
11:30	6
```

The left column represents the Y axis values, the right, the  X axis values. The first row is meta data only. The actual X,Y labels will be determined by the X and Y label fields in the post configuration.


## Technical Notes

This section outlines the internal architecture of this plugin. Before making any changes to the plugin, consult this documentation to be sure you understand how it works. 

### Overview

The design of the visualization plugin is fairly straightforward: 

* The customization post type is configured so that the only content on the rendered page results from the meta data defined in the custom fields and the data that are attached to the post; 
* The attachment's data are made available to the rendered page through a web service as JSON data; 
* These JSON data are then used to construct the charts using the chartjs library. 

The design emphasizes encapsulation, separation of concerns, and code readability. 

### functions.php

As is the case for any wordpress plugin, functions.php sets up the action and filter hooks. Almost all of these hooks either call class methods for classes that live in the class folder or load javascript that lives in the js folder. 


### tpl.visualization.php

The template file generates the html that appears on the page. It includes and consumes a number of the classes that are explained below. It is structured in such a way as to distinguish between the necessary data and the view of that data. Accordingly, the first half of the page consumes the included classes to build up the relevant data while the second half uses PHP's template semantics to build the html.

### PHP Classes

#### VisualizationTemplateBuilder

This class was originally developed in order to associate the visualization template with arbitrary page posts. As it turned out, it made more sense to use a custom post type for visualization. The visualization template is associated with the visualization post type by the "template_include" filter in functions.php. For now, then, VisualizationTemplateBuilder is not used at all. 

#### FieldEditor

This class manages the meta data for a visualization post and generates the fields that are used for these meta data on the admin page for the post. Several of its methods are associated with meta-data related actions in functions.html

#### FileDownload

This class generates the link with which a user can download the attachment for a visualization post. It also handles parsing the uri arguments when the user clicks the link and takes over the http response so as to cause an automatic download. As with the other classes, it is associated with actions in functions.php. 

#### VisualizationPostType

This class creates the visualization post type and the taxonomy associated with it. 

#### RestController

The RestController class sets up the REST method consumed by the visualization post page. It contains a single GET method:

````
http://<site-domain>/wp-json/clo-visualization/v1/datasets/{postId}
````

This method will consume the VisualizationData::get_xy_dataset() method if the post's Visualization Type is line or VisualizationData::get_grouped_dataset() for all other chart types. These methods return hierarchically organized associative arrays that are intended to be rendered as intelligibly structured JSON (see example above) with WordPress's default serialization mechanism. In other words, the rest method can return the raw results of the VisualizationData class's methods. 

#### VisualizationData

 This class addresses two core areas of functionality: 
 
 * Retrieval of meta data associated with posts and their attachments; 
 * Generation of JSON-ready hierarchical associative arrays for to be returned via the web service to the web client.
 
 There are extensive comments on the methods in this class. 

### Javascript Modules

#### chart-builder.js

This file exports the CLO_CHART_BUILDER module, which has one public function, getDataset(). This function takes a json object as a parameter. The json object defines a number of options that the module requires in order to render the chart and to provide user options for selecting data. Many of these options are determined by variables in the tpl.visualization.php class. Thus, the module is initialized as follows in tpl.visualization.php: 

````
    <script>
        window.onload = function () {

            CLO_CHART_BUILDER.getDataset(null,
                null,
                {
                    datsetUrlStem: '/wp-json/clo-visualization/v1/datasets/',
                    postId: <?= $post_id ?>,
                    cavnasArea: '<?= $canvas_area ?>',
                    groupInputSelectorName: '<?= $group_input_name ?>',
                    legendArea: '<?=$legend_area ?>',
                    visTypeBarVertical: '<?=FLD_TXT_BAR_VERTICAL?>',
                    visTypeBarHorizontal: '<?=FLD_TXT_BAR_HORIZONTAL?>',
                    visTypePie: '<?=FLD_TXT_PIE?>',
                    visTypeLine: '<?=FLD_TXT_LINE?>',
                    downloadImageElementId: '<?=$download_image_element_id?>',
                    displayLegend: <?= $display_legend == 1 ? 'true' : 'false' ?>
                }
            );
        };
    </script>

````


The intent of this approach is for the CLO_CHART_BUILDER module to operate as independently as it can from the page that is consuming it. In theory, it could be used by other pages besides tpl.visualization.php. In addition, by defining parameters such as css classes and element ids once in variables, the risk of error by mistyping these names is avoided. They can also be changed later with impunity.

#### admin-fields.js

This file exports the CLO_ADMIN_FIELDS module, which provides one initialization method, setUp(). It is initialized in the print_custom_fields() method of the FieldEditor class as follows: 

````
        print '<script>
        jQuery( document ).ready(function() {   
            CLO_ADMIN_FIELDS.setUp( 
                   {
                        fldIdVisType: "' . self::make_field_id(VISUALIZATION_TYPE) . '",
                        fldIdXAxisLabel: "' . self::make_field_id(X_AXIS_LABEL) . '",
                        fldIdYAxisLabel: "' . self::make_field_id(Y_AXIS_LABEL) . '",
                        fldIdDataSelectLabel: "' . self::make_field_id(DATA_SELECT_LABEL) . '",
                        fldIdSelectionType: "' . self::make_field_id(SELECTION_TYPE) . '",
                        fldIdBarStyle: "' . self::make_field_id(BAR_STYLE) . '",
                        classFormField: "' . FORM_FIELD_CLASS . '",
                        visTypeBarVertical:"' . FLD_TXT_BAR_VERTICAL . '",
                        visTypeBarHorizontal:"' . FLD_TXT_BAR_HORIZONTAL . '",
                        visTypePie:"' . FLD_TXT_PIE . '",
                        visTypeLine:"' . FLD_TXT_LINE . '",
                   }
                );
        });
        </script>';

````

Thus initialized, this module will hide and unhide different meta data controls on a post's admin page based on the Visualization Type (e.g., the X,Y axis label fields are not displayed for a pie chart). The parameterization of css class names, etc., is primarily to avoid typos and to allow for changing these names in the future (it is not anticipated that this code will be re-used by other plugins).


## Future Functionality

* Multiple Datasets for Line Charts: In the current design, the line chart visualization type allows for one attachment and one corresponding dataset; it would be useful to allow multiple attachments and for the user to have the ability to add/remove lines to the chart based on the datasets (similar to how bar and pie charts work now).
* Shortcodes: In the current design, a visualization functionality is provided only through distinct post type. As such it is its own page and can only be linked to from other pages and navigation mechanisms; it might be useful for users to have the option of including a visualization in a normal post via a shortcode so that they could integrate the visualization; 
* Scatter Chart: The scatter chart is a more accurate way of plotting x,y data because it does not interpolate lines between data points; 
* Histogram vs. Line: When a line chart is used, it might be useful to allow the user to switch between the line representation of the data and a bar chart representation. 

## Known Issues
* If you have created a bar chart of Visualization Type "Bar Chart," and set the Selection Type to "By Member," and then change the Visualization Type to "Pie Chart," when you try to display the page you will receive an error indicating that "Select By member" is not a valid option for Pie Charts. In order to change the Selection Type, you have to change the Visualization Type back to one fo the Bar Charts, and then switch back to Visualization Type "Pie Chart." Either the template side of the code should just ignore this setting for a pie chart, or the meta data mechanism should null out the current Selection Type when you switch to a new Visualization Type. 


# For More Information

For further information, send an email to clo-webadmin@cornell.edu.

