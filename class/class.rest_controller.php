<?php

include_once(plugin_dir_path(__FILE__) . "../class/class.visualization-data.php");


class RestController extends WP_REST_Controller
{

    /**
     * Register the routes for the objects of the controller.
     * Most of this is boilerplate from the example in https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
     * I am commenting out all of the example but leaving it here for now just so that in the future we can see what the various options are
     */
    public function register_routes()
    {
        $version = '1';
        $namespace = 'clo-visualization/v' . $version;
        $base = '';

        // this route will be available at http://birdcamslab03.dev.cc/wp-json/clo-visualization/v1/datasets/1
        register_rest_route($namespace,
            '/datasets/(?P<id>\d+)',
            array(array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_dataset'),
//                'permission_callback' => array($this, 'get_items_permissions_check'),
                'args' => array(),
            )
            ));


//        register_rest_route($namespace, '/' . $base, array(
//            array(
//                'methods' => WP_REST_Server::READABLE,
//                'callback' => array($this, 'get_items'),
//                'permission_callback' => array($this, 'get_items_permissions_check'),
//                'args' => array(),
//            ),
//            array(
//                'methods' => WP_REST_Server::CREATABLE,
//                'callback' => array($this, 'create_item'),
//                'permission_callback' => array($this, 'create_item_permissions_check'),
//                'args' => $this->get_endpoint_args_for_item_schema(true),
//            ),
//        ));
//        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)', array(
//            array(
//                'methods' => WP_REST_Server::READABLE,
//                'callback' => array($this, 'get_item'),
//                'permission_callback' => array($this, 'get_item_permissions_check'),
//                'args' => array(
//                    'context' => array(
//                        'default' => 'view',
//                    ),
//                ),
//            ),
//            array(
//                'methods' => WP_REST_Server::EDITABLE,
//                'callback' => array($this, 'update_item'),
//                'permission_callback' => array($this, 'update_item_permissions_check'),
//                'args' => $this->get_endpoint_args_for_item_schema(false),
//            ),
//            array(
//                'methods' => WP_REST_Server::DELETABLE,
//                'callback' => array($this, 'delete_item'),
//                'permission_callback' => array($this, 'delete_item_permissions_check'),
//                'args' => array(
//                    'force' => array(
//                        'default' => false,
//                    ),
//                ),
//            ),
//        ));
//        register_rest_route($namespace, '/' . $base . '/schema', array(
//            'methods' => WP_REST_Server::READABLE,
//            'callback' => array($this, 'get_public_item_schema'),
//        ));
    } // register_routes()




    /// for info on getting parameters,
    ///  see https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/#arguments
    ///
    ///
    ///

    public function get_dataset($request)
    {
        $return_val = null;


        $post_id = $request->get_param('id');

        if (!empty($post_id)) {

            $visualization_data = new VisualizationData();
            $error_message = "";

            $post_meta_data = $visualization_data->get_meta_data($post_id);
            $chart_type = $post_meta_data[VISUALIZATION_TYPE][0];

            if (!empty($chart_type)) {

                $client_json = Array();
                $chart_type = strtolower($chart_type);
                if ($chart_type == "pie" || strpos($chart_type, "bar") > 0) {

                    $client_json = $visualization_data->get_grouped_dataset($post_id, $error_message);

                } else if ($chart_type == "line") {
                    $client_json = $visualization_data->get_xy_dataset($post_id, $error_message);

                } else {
                    $error_message = "Unhandled chart type " . $chart_type;
                }

                if (empty($error_message)) {

                    if (count($client_json) > 0) {
                        $return_val = new WP_REST_Response($client_json, 200);
                    } else {
                        $return_val = new WP_Error(500, __("No json response was produced", 'text-domain'));
                    }

                } else {
                    $return_val = new WP_Error(500, __($error_message, 'text-domain'));
                }


            } else {
                $return_val = new WP_Error(500, __("There is no chart type defined for post " . $post_id, 'text-domain'));
            } // if-else chart type is available


        } else {
            // in theory I don't think this should ever happen because without the id segment
            // of the URL the request should not be routed ot this method
            $return_val = new WP_Error(500, __("id parameter is empty", 'text-domain'));
        }

        return $return_val;
    } // get_dataset()


//
//    /**
//     * Get a collection of items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Response
//     */
//    public function get_items($request)
//    {
//        $items = array(); //do a query, call another class, etc
//        $data = array();
//        foreach ($items as $item) {
//            $itemdata = $this->prepare_item_for_response($item, $request);
//            $data[] = $this->prepare_response_for_collection($itemdata);
//        }
//
//        return new WP_REST_Response($data, 200);
//    }
//
//    /**
//     * Get one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Response
//     */
//    public function get_item($request)
//    {
//        //get parameters from request
//        $params = $request->get_params();
//        $item = array();//do a query, call another class, etc
//        $data = $this->prepare_item_for_response($item, $request);
//
//        //return a response or error based on some conditional
//        if (1 == 1) {
//            return new WP_REST_Response($data, 200);
//        } else {
//            return new WP_Error('code', __('message', 'text-domain'));
//        }
//    }
//
//    /**
//     * Create one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function create_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_create_item')) {
//            $data = slug_some_function_to_create_item($item);
//            if (is_array($data)) {
//                return new WP_REST_Response($data, 200);
//            }
//        }
//
//        return new WP_Error('cant-create', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Update one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function update_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_update_item')) {
//            $data = slug_some_function_to_update_item($item);
//            if (is_array($data)) {
//                return new WP_REST_Response($data, 200);
//            }
//        }
//
//        return new WP_Error('cant-update', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Delete one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function delete_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_delete_item')) {
//            $deleted = slug_some_function_to_delete_item($item);
//            if ($deleted) {
//                return new WP_REST_Response(true, 200);
//            }
//        }
//
//        return new WP_Error('cant-delete', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Check if a given request has access to get items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function get_items_permissions_check($request)
//    {
//        //return true; <--use to make readable by all
//        return current_user_can('edit_something');
//    }
//
//    /**
//     * Check if a given request has access to get a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function get_item_permissions_check($request)
//    {
//        return $this->get_items_permissions_check($request);
//    }
//
//    /**
//     * Check if a given request has access to create items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function create_item_permissions_check($request)
//    {
//        return current_user_can('edit_something');
//    }
//
//    /**
//     * Check if a given request has access to update a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function update_item_permissions_check($request)
//    {
//        return $this->create_item_permissions_check($request);
//    }
//
//    /**
//     * Check if a given request has access to delete a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function delete_item_permissions_check($request)
//    {
//        return $this->create_item_permissions_check($request);
//    }
//
//    /**
//     * Prepare the item for create or update operation
//     *
//     * @param WP_REST_Request $request Request object
//     * @return WP_Error|object $prepared_item
//     */
//    protected function prepare_item_for_database($request)
//    {
//        return array();
//    }
//
//    /**
//     * Prepare the item for the REST response
//     *
//     * @param mixed $item WordPress representation of the item.
//     * @param WP_REST_Request $request Request object.
//     * @return mixed
//     */
//    public function prepare_item_for_response($item, $request)
//    {
//        return array();
//    }
//
//    /**
//     * Get the query params for collections
//     *
//     * @return array
//     */
//    public function get_collection_params()
//    {
//        return array(
//            'page' => array(
//                'description' => 'Current page of the collection.',
//                'type' => 'integer',
//                'default' => 1,
//                'sanitize_callback' => 'absint',
//            ),
//            'per_page' => array(
//                'description' => 'Maximum number of items to be returned in result set.',
//                'type' => 'integer',
//                'default' => 10,
//                'sanitize_callback' => 'absint',
//            ),
//            'search' => array(
//                'description' => 'Limit results to those matching a string.',
//                'type' => 'string',
//                'sanitize_callback' => 'sanitize_text_field',
//            ),
//        );
//    }
}
?>