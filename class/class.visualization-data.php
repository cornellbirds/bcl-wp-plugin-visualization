<?php

define("X_COL_IDX", "0");
define("Y_COL_IDX", "1");

define("KEY_DATASETS", "datasets");
define("KEY_DATASET_NAME", "dataset_name");
define("KEY_DATASET_TYPE", "dataset_type");
define("KEY_POST_META_DATA", "post_meta_data");
define("KEY_DATASET_TYPE_COORDINATES", "coordinates");
define("KEY_DATASET_TYPE_GROUPS", "groups");
define("KEY_DATASET_VALUES", "dataset_values");
define("KEY_DATASET_GROUP_NAME", "group_name");
define("KEY_DATASET_GROUP_VALUES", "group_values");
define("KEY_MEMBER_NAME", "member_name");
define("KEY_MEMBER_VALUE", "member_value");
define("KEY_COORDINATE_X", "x");
define("KEY_COORDINATE_Y", "y");


class VisualizationData
{



    /// Returns an associative array containing the meta data for the post
    public function get_meta_data($post_id)
    {

        // we may change how we do this later, so for now encapsulate
        return (get_post_custom($post_id));

    }

    /// Returns the post's meta data with the keys lower-cased
    private function add_meta_data($post_id, &$array_to_add_to)
    {
        $revised_meta_data = Array();

        $meta_data = $this->get_meta_data($post_id);
        foreach ($meta_data as $key => $curent_meta_data) {
            $revised_meta_data[strtolower($key)] = $curent_meta_data[0];
        }

        $array_to_add_to[KEY_POST_META_DATA] = $revised_meta_data;
    }

    /// This method gets the meta data for the post's attachment, which is the
    /// file that provides the data from which the visualization is generated.
    /// (Note the distinction between the post and the attachment associated with the post.)
    private function get_attachment_meta_data($parent_post_id)
    {

        $return_val = null;

        $attachments = get_children(array(
            'post_parent' => $parent_post_id,
            'post_type' => 'attachment',
            'numberposts' => -1,
            'post_mime_type' => 'text/csv'
        ));


        if (count($attachments) == 1) {
            $return_val = $attachments[array_keys($attachments)[0]];
        }

        return $return_val;
    }


    // Extracts the name of the attachment file from the attachment meta data
    public function get_attachment_name($parent_post_id)
    {

        $return_val = null;

        $attachment = $this->get_attachment_meta_data($parent_post_id);

        if ($attachment != null) {
            $return_val = $attachment->post_name;
        }

        return $return_val;
    }

    // Extracts the ID of the attachment file from the attachment meta data
    public function get_attachment_id($parent_post_id)
    {
        $return_val = null;

        $attachment = $this->get_attachment_meta_data($parent_post_id);

        if ($attachment != null) {
            $return_val = $attachment->ID;
        }

        return $return_val;

    } // get_attachment_id()



    // This class encapsulates common file processing functionality, such that you can pass in
    // a call back function that will be run on the file
    public function process_attachment($attachment_id, &$post_errors, $process_function)
    {

        $file_path = get_attached_file($attachment_id, false);


        if ($file_path != '') {

            if (($handle = fopen($file_path, "r")) !== FALSE) {

                $process_function($handle);

                fclose($handle);

            } else {
                $post_errors = "Unable to open file " . $file_path;
            }// if-else the file could be opened

        } else {

            $post_errors = "Unable to retrieve a file path for file with attachment id " . $attachment_id;

        }// if-else  there is a file path


    } // process_attachment()


    // This method returns a hierarchical associative array for data in an x,y format.
    // The structure of the array is intended to result in intelligibly formatted JSON
    // when it is returned to the client by the web service. For example:
    //
    // {
    //   "post_meta_data":{
    //      "wpa_off":"",
    //      "_edit_last":"1",
    //      "data_explanation":"This chart illustrates the number of displacements that took place at a given hour of the day.",
    //      "data_select_label":"",
    //      "visualization_type":"Line",
    //      "x_axis_label":"Hour of Day",
    //      "y_axis_label":"Number of Displacements",
    //      "_edit_lock":"1552408464:1"
    //   },
    //   "datasets":[
    //      {
    //         "dataset_name":"displacements-by-hour-of-day-reworked-from-spreadsheet_current",
    //         "dataset_type":"coordinates",
    //         "dataset_values":[
    //            {
    //               "x":"9",
    //               "y":"28"
    //            },
    //            {
    //               "x":"10",
    //               "y":"62"
    //            },
    //            {
    //               "x":"11",
    //               "y":"40"
    //            },
    //            {
    //               "x":"12",
    //               "y":"41"
    //            },
    //            {
    //               "x":"13",
    //               "y":"94"
    //            },
    //            {
    //               "x":"14",
    //               "y":"42"
    //            },
    //            {
    //               "x":"15",
    //               "y":"87"
    //            },
    //            {
    //               "x":"16",
    //               "y":"15"
    //            },
    //            {
    //               "x":"17",
    //               "y":""
    //            }
    //         ]
    //      }
    //   ]
    //}
    //
    //
    public function get_xy_dataset($parent_post_id, &$post_errors)
    {

        $return_val = Array();
        $current_dataset = Array();

        $attachment_id = $this->get_attachment_id($parent_post_id);

        if (!empty($attachment_id)) {

            $current_dataset[KEY_DATASET_NAME] = $this->get_attachment_name($parent_post_id);
            $current_dataset[KEY_DATASET_TYPE] = KEY_DATASET_TYPE_COORDINATES;
            $current_dataset[KEY_DATASET_VALUES] = Array();

            $this->process_attachment($attachment_id,
                $post_errors,
                function ($handle) use (&$current_dataset) {

                    $meta_data_row_count = 1;
                    $meta_data_row_start_idx = 0;

                    $label_row_start_idx = $meta_data_row_start_idx + $meta_data_row_count;
                    $data_row_start_idx = $label_row_start_idx + 1;

                    $x_col_idx = intval(X_COL_IDX);
                    $y_col_idx = intval(Y_COL_IDX);

                    $current_csv_row_num = 0;

                    while (($current_csv_row_data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                        if ($current_csv_row_num > $data_row_start_idx) {

                            $current_x_val = $current_csv_row_data[$x_col_idx];
                            $current_y_val = $current_csv_row_data[$y_col_idx];
                            $current_xy_array = Array();
                            $current_xy_array[KEY_COORDINATE_X] = $current_x_val;
                            $current_xy_array[KEY_COORDINATE_Y] = $current_y_val;
                            array_push($current_dataset[KEY_DATASET_VALUES], $current_xy_array);


                        } // if we've moved over the meta data row(s)

                        $current_csv_row_num++;

                    } // iterate file contents

                });

        } else {

            $post_errors = "There is no attachment for parent post " . $parent_post_id;
        }

        $this->add_meta_data($parent_post_id, $return_val);
        $return_val[KEY_DATASETS] = Array();
        array_push($return_val[KEY_DATASETS], $current_dataset);

        return $return_val;

    } // get_xy_dataset()




    // This method returns a hierarchical associative array for data in grouped format.
    // These data are used for all chart types except for line. The distinction between
    // groups and members is vital to the rendering of charts.
    //
    // ANY MODIFICATIONS TO THE STRUCTURE OF THIS METHOD'S RETURN VALUE WILL HAVE SIGNIFICANT
    // CONSEQUENCES IN THE WEB CLIENT AND ELSEWHERE!
    //
    // The structure of the array is intended to result in intelligibly formatted JSON
    // when it is returned to the client by the web service. For example:
    //
    // {
    //   "datasets":[
    //      {
    //         "dataset_name":"displacements-by-species-reworked-from-spreadsheet_current_copy_02",
    //         "dataset_type":"groups",
    //         "dataset_values":[
    //            {
    //               "group_name":"American Goldfinch",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"58"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"5"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Bluejay",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"21"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"4"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"European Starling",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"4"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"84"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"10"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"2"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Mourning Dove",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"10"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"29"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Redwinged Blackbird",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Downy Woodpecker",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"2"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Tufted Titmouse",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"House Finch",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"5"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"2"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Northern Cardinal",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"2"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Red Bellied Woodpecker",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"American Tree Sparrow",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"1"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            },
    //            {
    //               "group_name":"Has Zero",
    //               "group_values":[
    //                  {
    //                     "member_name":"American Goldfinch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Bluejay",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"European Starling",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Mourning Dove",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Redwinged Blackbird",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Downy Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Tufted Titmouse",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"House Finch",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Northern Cardinal",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Red Bellied Woodpecker",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"American Tree Sparrow",
    //                     "member_value":"0"
    //                  },
    //                  {
    //                     "member_name":"Has Zero",
    //                     "member_value":"0"
    //                  }
    //               ]
    //            }
    //         ]
    //      }
    //   ],
    //   "post_meta_data":{
    //      "wpa_off":"",
    //      "_edit_last":"1",
    //      "data_explanation":"This chart compares compares displacements by source across all sources, in absolute numbers.",
    //      "data_select_label":"Add or remove source species",
    //      "visualization_type":"Vertical Bar",
    //      "x_axis_label":"Source Spcies",
    //      "y_axis_label":"Number of Displacements",
    //      "_edit_lock":"1553631397:1",
    //      "selection_type":"By Group",
    //      "bar_style":"Stacked"
    //   }
    //}
    //
    //The file to be processed by this method must have the following structure:
    //        Member Names:,Group Names:,,,,,,
    //        ,group 1,group 2,group 3,group 4,group 5,group 6,group 7
    //        member 1,58,21,4,1,0,0,0
    //        member 2,0,0,0,0,0,1,1
    //        member 3,0,4,84,10,0,2,0
    //        member 4,0,1,10,29,1,0,0
    //
    //In other words, each group is formed by a column, where the column header is the group name, and the data are the remaining rows in that column;
    //The member name for each data row are determined by the first column in the file
    public function get_grouped_dataset($parent_post_id, &$post_errors)
    {

        $return_val = Array();

        $current_dataset = Array();

        $attachment_id = $this->get_attachment_id($parent_post_id);
        if (!empty($attachment_id)) {

            $current_dataset[KEY_DATASET_NAME] = $this->get_attachment_name($parent_post_id);
            $current_dataset[KEY_DATASET_TYPE] = KEY_DATASET_TYPE_GROUPS;
            $current_dataset[KEY_DATASET_VALUES] = Array();

            $this->process_attachment($attachment_id,
                $post_errors,
                function ($handle) use (&$current_dataset) {

                    // for now, these meta-data coordinates are "hard-coded" -- the input file format must cohere to these
                    $meta_data_row_count = 1;
                    $group_name_row_start_idx = 0;
                    $meta_data_col_count = 1;
                    $member_name_column_idx = 0;


                    // calculated values from meta-data coordinates
                    $label_row_start_idx = $group_name_row_start_idx + $meta_data_row_count;
                    $data_column_start_idx = $member_name_column_idx + 1;

                    $group_names = Array();
                    $current_csv_row_num = 0;

                    while (($current_csv_row_data = fgetcsv($handle, 1000, ",")) !== FALSE) {


                        if ($current_csv_row_num == 1) {

                            // capture group names from first meta data row, removing the member label column
                            $group_names = $current_csv_row_data;
                            array_splice($group_names, $member_name_column_idx, $meta_data_col_count);


                        } else if ($current_csv_row_num > 1) {

                            // now create value groups from the data rows, removing the member label column
                            $current_values_array_from_csv_row = $current_csv_row_data;
                            array_splice($current_values_array_from_csv_row, $member_name_column_idx, $data_column_start_idx);

                            // note that the $current_values_array and $group names are now aligned so you can get
                            // the current group name using the index of the current value column

                            for ($current_values_idx = 0;
                                 $current_values_idx < sizeof($current_values_array_from_csv_row);
                                 $current_values_idx++) {

                                $current_group_name = $group_names[$current_values_idx];

                                $current_values = Array();
                                $current_member_name = $current_csv_row_data[$member_name_column_idx];
                                $current_values[KEY_DATASET_GROUP_VALUES][KEY_MEMBER_NAME] = $current_member_name;

                                $current_values_array_value = $current_values_array_from_csv_row[$current_values_idx];
                                $current_values[KEY_DATASET_GROUP_VALUES][KEY_MEMBER_VALUE] = $current_values_array_value;


                                $found_group_array = false;
                                for ($idx = 0;
                                     ($found_group_array == false) && ($idx < count($current_dataset[KEY_DATASET_VALUES]));
                                     $idx++) {

                                    // note: if you don't do this by reference, your values don't end up in the destination array!
                                    $current_values_from_dataset = &$current_dataset[KEY_DATASET_VALUES][$idx];

                                    if ($current_values_from_dataset[KEY_DATASET_GROUP_NAME] == $current_group_name) {

                                        $found_group_array = true;
                                        $values_for_group_array = Array();
                                        $values_for_group_array[KEY_MEMBER_NAME] = $current_member_name;
                                        $values_for_group_array[KEY_MEMBER_VALUE] = $current_values_array_value;
                                        array_push($current_values_from_dataset[KEY_DATASET_GROUP_VALUES], $values_for_group_array);

                                    }

                                }

                                if ($found_group_array == false) {

                                    $group_array = Array();
                                    $group_array[KEY_DATASET_GROUP_NAME] = $current_group_name;
                                    $group_array[KEY_DATASET_GROUP_VALUES] = Array();

                                    $values_array = Array();
                                    $values_array[KEY_MEMBER_NAME] = $current_member_name;
                                    $values_array[KEY_MEMBER_VALUE] = $current_values_array_value;
                                    array_push($group_array[KEY_DATASET_GROUP_VALUES], $values_array);

                                    array_push($current_dataset[KEY_DATASET_VALUES], $group_array);
                                }

                            } // iterate current row's displacement data

                        } // if else we're on the first row

                        $current_csv_row_num++;


                    } // iterate file contents

                });

            $return_val[KEY_DATASETS] = Array();
            array_push($return_val[KEY_DATASETS], $current_dataset);

        } else {

            $post_errors = "There is no attachment for parent post " . $parent_post_id;
        }

        $this->add_meta_data($parent_post_id, $return_val);


        return $return_val;

    } // get_grouped_dataset()


    // returns an array of unique member names across all groups in the dataset
    public function get_members_for_grouped_dataset($parent_post_id, &$post_errors)
    {
        $return_val = Array();

        $grouped_dataset = $this->get_grouped_dataset($parent_post_id, $post_errors);

        if (empty($grouped_dataset) == false) {

            foreach ($grouped_dataset[KEY_DATASETS][0][KEY_DATASET_VALUES] as $current_dataset_group) {

                foreach ($current_dataset_group[KEY_DATASET_GROUP_VALUES] as $current_group_values) {


                    $current_member_name = $current_group_values[KEY_MEMBER_NAME];
                    if (!in_array($current_member_name, $return_val)) {

                        array_push($return_val, $current_member_name);
                        break;
                    }

                }  // iterate group values

            } // iterate groups

        } // if we got a grouped data set

        return $return_val;
    }


    // returns an array of all group names in the dataset
    public function get_groups_for_grouped_dataset($parent_post_id, &$post_errors)
    {
        $return_val = Array();

        $get_grouped_errors = "";
        $grouped_dataset = $this->get_grouped_dataset($parent_post_id, $get_grouped_errors);

        if( empty($get_grouped_errors)) {
            if (empty($grouped_dataset) == false) {

                foreach ($grouped_dataset[KEY_DATASETS][0][KEY_DATASET_VALUES] as $current_dataset_group) {

                    array_push($return_val, $current_dataset_group[KEY_DATASET_GROUP_NAME]);

                    // REINSTATE IF WE WANT TO FILTER RESULT
//                foreach ($current_dataset_group[KEY_DATASET_GROUP_VALUES] as $current_group_values)
//
//                    if (intval($current_group_values[KEY_MEMBER_VALUE]) > 0) {
//                        array_push($return_val, $current_dataset_group[KEY_DATASET_GROUP_NAME]);
//                        break;
//                    }
                }
            } // if we got a grouped data set
        } else {
            $post_errors = $get_grouped_errors;
        }

        return $return_val;
    }

}

?>