<?php

include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/interface.field_configuration.php');
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/class.field_utils.php');

define("AXIS_LABEL_SIZE", '40');


class FieldConfigurationVisualization implements iFieldConfiguration
{
    public function get_field_definitions()
    {
        return (

        array(
            array(
                FLD_DEF_KEY_NAME => VISUALIZATION_TYPE,
                FLD_DEF_KEY_TITLE => 'Visualization Type',
                FLD_DEF_KEY_DESC => 'Choose a visualization type',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_DROPDOWN,
                FLD_DEF_KEY_OPTIONS => array(FLD_TXT_BAR_VERTICAL, FLD_TXT_BAR_HORIZONTAL, FLD_TXT_PIE, FLD_TXT_LINE),
            ),
            array(
                FLD_DEF_KEY_NAME => DATA_SELECT_LABEL,
                FLD_DEF_KEY_TITLE => 'Data select label',
                FLD_DEF_KEY_DESC => 'The text that will appear with the control by which the user includes or excludes data from the visualization',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
                FLD_DEF_KEY_SIZE => 25
            ),
            array(
                FLD_DEF_KEY_NAME => BAR_STYLE,
                FLD_DEF_KEY_TITLE => 'Bar Style',
                FLD_DEF_KEY_DESC => 'Choose Bar Style',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_DROPDOWN,
                FLD_DEF_KEY_OPTIONS => array('Stacked', 'Grouped'),
            ),
            array(
                FLD_DEF_KEY_NAME => SELECTION_TYPE,
                FLD_DEF_KEY_TITLE => 'Selection Type',
                FLD_DEF_KEY_DESC => 'Choose a selection type',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_DROPDOWN,
                FLD_DEF_KEY_OPTIONS => array('By Group', 'By Member'),
            ),
            array(
                FLD_DEF_KEY_NAME => X_AXIS_LABEL,
                FLD_DEF_KEY_TITLE => 'X Axis Label',
                FLD_DEF_KEY_DESC => 'Specify X Axis label',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
                FLD_DEF_KEY_SIZE => AXIS_LABEL_SIZE
            ),
            array(
                FLD_DEF_KEY_NAME => Y_AXIS_LABEL,
                FLD_DEF_KEY_TITLE => 'Y Axis Label',
                FLD_DEF_KEY_DESC => 'Specify Y Axis label',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
                FLD_DEF_KEY_SIZE => AXIS_LABEL_SIZE
            ),
        )

        );
    } // getFieldDefinitions()

    public function get_fields_to_remove()
    {
        return (array("postcustom", "customsidebars-mb"));
    }

    public function get_post_type()
    {
        return (VISUALIZATION_POST_TYPE_NAME);
    }

    public function get_meta_box_id()
    {
        return ('visualization-fields');
    }

    public function get_meta_box_title()
    {
        return ('Configure Visualization');
    }

    public function get_field_prefix()
    {
        return ('VIS_FIELD_');
    }


    public function get_admin_page_script()
    {
        // sadly, there does not seem to be a way to call a function with parameters in
        // a heredoc. So we are left to store the values ahead of time and then put them
        // into the heredoc. What we get in return is that the heredoc is a lot more readable
        // than a long series of string concatenations.
        $fldIdVisType = FieldUtils::make_field_id($this, VISUALIZATION_TYPE);
        $fldIdXAxisLabel = FieldUtils::make_field_id($this, X_AXIS_LABEL);
        $fldIdYAxisLabel = FieldUtils::make_field_id($this, Y_AXIS_LABEL);
        $fldIdDataSelectLabel = FieldUtils::make_field_id($this, DATA_SELECT_LABEL);
        $fldIdSelectionType = FieldUtils::make_field_id($this, SELECTION_TYPE);
        $fldIdBarStyle = FieldUtils::make_field_id($this, BAR_STYLE);
        $classFormField = FORM_FIELD_CLASS;
        $visTypeBarVertical = FLD_TXT_BAR_VERTICAL;
        $visTypeBarHorizontal = FLD_TXT_BAR_HORIZONTAL;
        $visTypePie = FLD_TXT_PIE;
        $visTypeLine = FLD_TXT_LINE;


        // the closing tag of EOD must be the first and _only_ thing terminating the incline doc (you cannot even add a comment to that line!)
        $return_val =
            <<<EOD
            <script>
                jQuery( document ).ready(function() {   
                    CLO_ADMIN_FIELDS.setUp( 
                           {
                                fldIdVisType:  '$fldIdVisType' ,
                                fldIdXAxisLabel:  '$fldIdXAxisLabel' ,
                                fldIdYAxisLabel:  '$fldIdYAxisLabel',
                                fldIdDataSelectLabel:  '$fldIdDataSelectLabel',
                                fldIdSelectionType:  '$fldIdSelectionType',
                                fldIdBarStyle:  '$fldIdBarStyle' ,
                                classFormField:  '$classFormField',
                                visTypeBarVertical: '$visTypeBarVertical',
                                visTypeBarHorizontal: '$visTypeBarHorizontal',
                                visTypePie: '$visTypePie',
                                visTypeLine: '$visTypeLine' ,
                           }
                        );
                });
            </script>
EOD;


        return ($return_val);

    }


    public function get_is_hide_editor()
    {
        return false;
    }

} // FieldConfigurationVisualization