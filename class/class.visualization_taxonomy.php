<?php

/**
 * VisualizationPostType
 *
 */
class VisualizationTaxonomy
{

    public static function register_visualization_taxonomy()
    {

        register_taxonomy(
            VISUALIZATION_POST_TYPE_TAXONOMY_NAME,
            VISUALIZATION_POST_TYPE_NAME,
            array(
                'label' => __(VISUALIZATION_POST_TYPE_TAXONOMY_NAME_LABEL),
//                'rewrite' => array('slug' => VISUALIZATION_POST_TYPE_TAXONOMY_NAME),
                'hierarchical' => false,
                'show_ui' => true,
            )
        );


        register_taxonomy_for_object_type(VISUALIZATION_POST_TYPE_TAXONOMY_NAME, VISUALIZATION_POST_TYPE_NAME);

    } //register_study_taxonomy

    public static function get_pages_of_same_taxonomy_type($post_id, &$post_errors)
    {

        $return_val = Array();

        // this query does not, I think, detect a case in which there are multiple tags associated with a post,
        // where the tags are organized hierarchically. See w-151. Rather than try to fix the query, I have simply
        // set the hierarchical flag to false in register_visualization_taxonomy()
        $post_terms = wp_get_post_terms($post_id, VISUALIZATION_POST_TYPE_TAXONOMY_NAME);

        if (count($post_terms) == 1) {

            $study_name = $post_terms[0]->name; // this will have to be programmatically determined eventually
            $study_page_args = array(

                'post_type' => VISUALIZATION_POST_TYPE_NAME,
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'taxonomy' => VISUALIZATION_POST_TYPE_TAXONOMY_NAME,
                'field' => 'slug',
                'term' => $study_name,
                'posts_per_page' => -1 
            );

            $return_val = (new WP_Query($study_page_args))->posts;
        } else {

            $post_errors = "There should be one term of type " . VISUALIZATION_POST_TYPE_TAXONOMY_NAME . "; found " . count($post_terms);
            $post_errors = $post_errors . "\n" . print_r($post_terms, true);

        }

        return $return_val;

    } // get_pages_of_same_taxonomy_type()

} // class
?>