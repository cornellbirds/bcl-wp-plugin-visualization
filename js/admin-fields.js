CLO_ADMIN_FIELDS = (function () {

    let configInit = function( fieldConfig) {

        jQuery("#" + fieldConfig.fldIdXAxisLabel).hide();
        jQuery("#" + fieldConfig.fldIdYAxisLabel).hide();
        jQuery("#" + fieldConfig.fldIdDataSelectLabel).hide();
        jQuery("#" + fieldConfig.fldIdSelectionType).hide();
        jQuery("#" + fieldConfig.fldIdBarStyle).hide();

    }

    let configBarType = function(fieldConfig) {

        jQuery("#" + fieldConfig.fldIdXAxisLabel).show();
        jQuery("#" + fieldConfig.fldIdYAxisLabel).show();
        jQuery("#" + fieldConfig.fldIdDataSelectLabel).show();
        jQuery("#" + fieldConfig.fldIdSelectionType).show();
        jQuery("#" + fieldConfig.fldIdBarStyle).show();

    };

    let configPieType = function(fieldConfig) {

        jQuery("#" + fieldConfig.fldIdXAxisLabel).hide();
        jQuery("#" + fieldConfig.fldIdYAxisLabel).hide();
        jQuery("#" + fieldConfig.fldIdDataSelectLabel).show();
        jQuery("#" + fieldConfig.fldIdSelectionType).hide();
        jQuery("#" + fieldConfig.fldIdBarStyle).hide();

    };

    let configLineType = function(fieldConfig) {

        jQuery("#" + fieldConfig.fldIdXAxisLabel).show();
        jQuery("#" + fieldConfig.fldIdYAxisLabel).show();
        jQuery("#" + fieldConfig.fldIdDataSelectLabel).hide();
        jQuery("#" + fieldConfig.fldIdSelectionType).hide();
        jQuery("#" + fieldConfig.fldIdBarStyle).hide();

    };


    let configControls = function(selectedType,fieldConfig) {

        switch (selectedType) {

            case fieldConfig.visTypeBarVertical:
                configBarType(fieldConfig);
                break;

            case fieldConfig.visTypeBarHorizontal:
                configBarType(fieldConfig);
                break;

            case fieldConfig.visTypePie:
                configPieType(fieldConfig);
                break;

            case fieldConfig.visTypeLine:
                configLineType(fieldConfig);
                break;

            default:
                configInit(fieldConfig);


        }
    };


    let setUp = function (fieldConfig) {

        configControls(jQuery("select#" + fieldConfig.fldIdVisType).children("option:selected").val(), fieldConfig);

        jQuery("select#" + fieldConfig.fldIdVisType).change(function (event) {

            //console.log(fieldConfig.fldIdXAxisLabel);
            let selectedVisualizationType = this.value;
            configControls(selectedVisualizationType, fieldConfig);

        });

    }; //fieldConfig()


    // return determines the "public" methods of the module
    return {
        setUp: setUp
    }

})();
