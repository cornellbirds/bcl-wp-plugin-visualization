CLO_CHART_BUILDER = (function () {


        var currentChart = null;

        // In case more colors are needed: https://www.rapidtables.com/web/color/RGB_Color.html
        var chartColors = {
            black: 'rgb(0,0,0)',
            red: 'rgb(237,28,36)',
            brown: 'rgb(167,114,55)',
            darkblue: 'rgb(0,73,124)',
            darkorange: 'rgb(140,79,1)',
            midgreen: 'rgb(105,145,109)',
            gray: 'rgb(99,100,102)',
            darkred: 'rgb(139,3,4)',
            gold: 'rgb(190,165,1)',
            olive: 'rgb(139,151,22)',
            orange: 'rgb(242,141,29)',
            natural: 'rgb(151,137,129)',
            green: 'rgb(0,71,18)',
            lightblue: 'rgb(27,177,231)',
            blue: 'rgb(0,104,169)',
        };


        var colorMap = null;


        var getColors = function (dataset) {

            if (!colorMap) {

                let allMembernames =
                    Array.from(
                        new Set(
                            dataset
                                .map(groupItem =>
                                    groupItem.group_values
                                        .map(memberItem => memberItem.member_name)
                                        .join()
                                ).join()
                                .split(",")
                        ));

                let colorList = Object.values(Object.assign(chartColors));

                if (allMembernames.length < colorList.length) {
                    colorList.splice(0, (colorList.length - allMembernames.length));
                }

                colorMap = {};
                colorList.forEach((color, index) => colorMap[allMembernames[index]] = color);
            }

            return colorMap;

        };

        // chart legend html callback
        function legendHtmlCallback(chart) {

            let text = [];

            if (colorMap) { // typically getColors() will have been called before the chart legend is formed

                let labels = [];
                let colors = [];
                // we're only dealing with pie and 'everything else'; the truth is that this function does not even seem to get
                // called for a line chart. But since we don't support multi-dataset line charts yet, it doesn't matter: there
                // won't be a need to correlate colors with labels
                // The reason we need to treat pie charts differently from bar chars, here, has something to do with the way the
                // data are structured. In a bar chart, there are multiple datasets, one for each member, where each dataset's data
                // array corresponds to the value of the member for each group, in order as the groups are ordered in the data.labels array
                // In this case, the order of the labels is arbitrary, but if they are not always sorted, they end up sorting randomly
                // when the legend is redrawn as groups are added and removed.
                // In the pie chart, on the other hand, there is only one dataset: backgroundColor is an array of colors, where the
                // order of colors corresponds to the members listed in the data.labels array. The label in the dataset is ignored.
                // Here we need to preserve the order of the labels as they are listed in data.labels. Otherwise, when you mouseover
                // a slice of the pie, the color and label do not correspond to what is listed in the legend.
                if (chart.config.type === "pie") {

                    labels = chart.config.data.labels;
                    colors = chart.config.data.datasets[0].backgroundColor; // there should only ever be one dataset in a pie chart configuration

                } else {
                    labels = chart.config.data.datasets.map(currentDataset => currentDataset.label);
                    labels.sort();
                    labels.forEach(currentLabel => colors.push(colorMap[currentLabel]));
                }

                text.push('<ul class="' + chart.id + '-legend">');
                for (var idx = 0; idx < labels.length; idx++) {

                    let currentLabel = labels[idx];
                    let currentColor = colors[idx];

                    if (currentLabel && currentColor) {
                        text.push('<li><div class="legendValue">');
                        text.push('<span style="background-color:' + currentColor + '">&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        text.push('<span class="label">' + currentLabel + '</span>');
                        text.push('</div></li>');
                    }
                }
                text.push('</ul>');
                // console.log(chart);
            } else {

                console.log("Unable to write member legend: colors have not been defined");
            }

            return text.join('');

        } // legendHtmlCallback()

        let getMemberNames = function (dataset, groupNames, selectByGroup, selectedCheckBoxIds) {

            // this is ugly and probably not particularly efficient, but it does
            // succeed at getting a unique list of all member names across all groups
            return (Array.from(
                new Set(
                    dataset
                        .filter(groupItem => groupNames.includes(groupItem.group_name))
                        .map(groupItem =>
                            groupItem.group_values
                                .filter(memberItem => (selectByGroup && parseInt(memberItem.member_value) !== null) || (selectedCheckBoxIds.find(item => item === memberItem.member_name)))
                                .map(memberItem => memberItem.member_name)
                                .join()
                        ).join()
                        .split(",")
                )));
        };

        // This function provides global configuration for all chart types
        var updateChart = function (chartConfig, pageConfig) {

            // build chart

            // set background color to all white, but with full opacity, so that the resulting png
            // will have a neutral (rather than transparent) background
            // This solution adapted from https://stackoverflow.com/a/38493678/2661408
            chartConfig.options.canvas = {
                backgroundColor: 'rgba(255, 255, 255, 1.0)',
                backgroundImage: "url('http://www.planwallpaper.com/static/images/comic-sunburst-background-o.jpg')"
            };
            Chart.pluginService.register({
                beforeDraw: function (chart, easing) {
                    var ctx = chart.chart.ctx;
                    if (chart.config.options.canvas && chart.config.options.canvas.backgroundColor) {
                            ctx.save();
                            ctx.fillStyle = chart.config.options.canvas.backgroundColor;
                            ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
                            ctx.restore();
                    }
                }
            });


            // if we do any legend at all, it will be our custom one
            chartConfig.options.legend = {
                display: false,
            };

            chartConfig.options.animation = {
                onComplete: function () {
                    let url = currentChart.toBase64Image("image/jpg"); //get image as base64
                    document.getElementById(pageConfig.downloadImageElementId).href = url;
                }
            };



            // chartConfig.canvas = {backgroundColor: 'rgb(226, 13, 13)'};
            // chartConfig.chartArea = {
            //     backgroundColor: 'rgba(30, 30, 30, 1)',
            // };

            var ctx = document.getElementById(pageConfig.cavnasArea).getContext('2d');
            currentChart = new Chart(ctx, chartConfig);


            let r_a = 0.3;
            ctx.fillStyle = "rgba(32, 45, 21, " + r_a + ")";
            // generate HTML legend
// console.log(currentChart);
// console.log(chartConfig);


            if( pageConfig.displayLegend) {
                chartConfig.options.legendCallback = legendHtmlCallback;
                var chartLegend = document.getElementById(pageConfig.legendArea);
                chartLegend.innerHTML = currentChart.generateLegend();
                currentChart.update();
            }

        };

        var getCheckBoxNameAttributes = function (retrieveAll, configOptions) {

            let checkBoxes = jQuery('input[name=' + configOptions.groupInputSelectorName + ']');
            let checkBoxArray = jQuery.makeArray(checkBoxes);
            let returnVal =
                checkBoxArray
                    .filter(item => retrieveAll || item.checked === true)
                    .map(item => item.id);

            return returnVal;
        }


        var makeLabelObject = function (labelText) {
            return {
                display: labelText,
                labelString: labelText,
                fontSize: 15,
                fontStyle: 'bold'
            };
        }

        var drawBarChart = function (dataset, pageConfig, barType, stacked, selectByGroup, showAll, xAxisLabel, yAxisLabel) {


            let groupNames = [];
            let allMemberNamesWithValues = [];
            let checkBoxIds = getCheckBoxNameAttributes(showAll, pageConfig);
            if (selectByGroup) {
                groupNames = getCheckBoxNameAttributes(showAll, pageConfig);
                allMemberNamesWithValues = getMemberNames(dataset, groupNames, selectByGroup, checkBoxIds);
            } else {
                // if you're selecting by member, by definition, you use all groups
                groupNames = dataset.map(item => item.group_name);
                allMemberNamesWithValues = getMemberNames(dataset, groupNames, selectByGroup, checkBoxIds);

            }


            let allMembersToColors = getColors(dataset);

            let barSegmentData = [];
            groupNames.forEach((currentGroupName, currentGroupIndex) => {

                let currentGroup = dataset.find(groupItem => groupItem.group_name === currentGroupName);

                allMemberNamesWithValues.forEach(currentMemberName => {


                    let currentMemberValue;
                    let currentMember = currentGroup.group_values.find(member => member.member_name === currentMemberName);
                    if (currentMember) {
                        currentMemberValue = currentMember.member_value;
                    } else {
                        currentMemberValue = 0;
                    }

                    let currentSegmentDef = barSegmentData.find(segment => segment.label === currentMemberName);
                    if (!currentSegmentDef) {

                        currentSegmentDef = {
                            label: currentMemberName,
                            data: [],
                            backgroundColor: allMembersToColors[currentMemberName]
                        };

                        barSegmentData.push(currentSegmentDef);
                    }

                    currentSegmentDef.data[currentGroupIndex] = currentMemberValue;

                }); // iterate member names

            }); // iterate groups


            var barChartData = {
                labels: groupNames,
                datasets: barSegmentData
            };

            let chartConfig = {
                type: barType,
                data: barChartData,
                options: {
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart - Stacked'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        filter: function (tooltipItem) {
                            return tooltipItem.yLabel !== 0;
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        xAxes: [{
                            scaleLabel: makeLabelObject(xAxisLabel),
                            stacked: stacked,
                            ticks: {
                                autoSkip: false,
                                maxRotation: 90,
                                minRotation: 90
                            }
                        }],
                        yAxes: [{
                            scaleLabel: makeLabelObject(yAxisLabel),
                            stacked: stacked
                        }]
                    },
                    layout: {
                        padding: {
                            bottom: 80
                        }
                    },
                }
            };

            chartConfig.options.layout = {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            };

            updateChart(chartConfig, pageConfig);

        }; // drawBarChart()


        var drawLineChart = function (xyData, xAxisLabel, yAxisLabel, chartTitle, pageConfig) {

            let xValues = xyData.map(item => item.x);
            let yValues = xyData.map(item => item.y);
            let chartConfig = {
                type: 'line',
                data: {
                    labels: xValues,
                    datasets: [{
                        label: chartTitle,
                        backgroundColor: chartColors.red,
                        borderColor: chartColors.red,
                        data: yValues,
                        fill: false,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: false,
                        text: 'Chart.js Line Chart'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: makeLabelObject(xAxisLabel),
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: makeLabelObject(yAxisLabel),
                        }]
                    },
                } // options
            };


            updateChart(chartConfig, pageConfig);

        }; //drawLineChart()

        var drawPieChart = function (dataset, groupName, pageConfig) {

            // console.log("Pie Chart dataset: ")
            // console.log(dataset);

            let datasetIdx;

            if (groupName) {

                let groupNames = dataset.map(item => item.group_name);
                datasetIdx = groupNames.indexOf(groupName);

            } else {

                datasetIdx = 0;

            }


            // All of the arrays used to configure the chart absolutely must
            // line up to the same number or strange things will happen
             let memberValues = dataset[datasetIdx]
                .group_values
                .filter(item => parseInt(item.member_value) > 0)
                .map(item => item.member_value);

            let memberNames = dataset[datasetIdx]
                .group_values
                .filter(item => parseInt(item.member_value) > 0)
                .map(item => item.member_name);


            let allColors = getColors(dataset);
            let colorsInUse = [];
            memberNames.forEach(memberName => colorsInUse.push(allColors[memberName]));

            let chartConfig = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: memberValues,
                        backgroundColor: colorsInUse,
                    }],
                    labels: memberNames
                },
                options: {
                    responsive: true,
                    plugins: {
                      outlabels: {
                        //backgroundColor: null,
                        //color: 'black',
                        font: {
                          resizeable: true,
                          minSize: 10,
                          maxSize: 14,
                          weight: 'bold',
                        },
                        lineWidth: 1,
                        stretch: 10,
                      }
                    }
                },
            };

            // chartConfig.options.pieceLabel = {
            //     render: 'label',
            //     fontColor: '#000',
            //     position: 'outside',
            //     segment: true
            // };

            chartConfig.options.tooltips = {
                enabled: false
            };

            chartConfig.options.layout = {
                padding: {
                    left: 60,
                    right: 60,
                    top: 70,
                    bottom: 40
                }
            };

            // chartConfig.options.plugins = {
            //     datalabels: {
            //         formatter: (value, ctx) => {
            //
            //             let datasets = ctx.chart.data.datasets;
            //
            //             if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
            //                 let sum = datasets[0].data.reduce((a, b) => a + b, 0);
            //                 let percentage = Math.round((value / sum) * 100) + '%';
            //                 return percentage;
            //             } else {
            //                 return percentage;
            //             }
            //         },
            //         color: '#fff',
            //     }
            // };

            updateChart(chartConfig, pageConfig);

        } // drawPieChart()

        var getDataset = function (selectedSpecies, callBack, configOptions) {

            if (!callBack) {
                callBack = setUpChart;
            }

            jQuery(function ($) {

                let datasetUrl = configOptions.datsetUrlStem + configOptions.postId;
                $.ajax({
                    url: datasetUrl,
                    method: "GET",
                    data: {
                        a: "a"
                    }
                }).done(function (data) {
                    callBack(data, selectedSpecies, configOptions);
                }).fail(function (xhr) {
                    console.log('error', xhr);
                });


            });
        }


        var setUpChart = function (data, selectedGroup, configOptions) {


            let dataset;
            let metaData = data.post_meta_data;
            let chartType = metaData.visualization_type && metaData.visualization_type;
            let selectionType = metaData.selection_type && metaData.selection_type.toLowerCase();
            let barChartStyle = metaData.bar_style && metaData.bar_style.toLowerCase();
            let xAxisLabel = metaData.x_axis_label;
            let yAxisLabel = metaData.y_axis_label;


//            let chartJsType = chartType === configOptions.visTypeBarVertical ? "bar" : "horizontalBar";

            if ((chartType === configOptions.visTypePie) || (chartType === configOptions.visTypeBarVertical) || (chartType === configOptions.visTypeBarHorizontal)) {

                dataset = data.datasets[0].dataset_values; // for now we only handle one dataset

                let groupNames = dataset.map(item => item.group_name);
                let checkBoxIds = getCheckBoxNameAttributes(true, configOptions);

                let inputControlIds = [];
                if (selectionType === "by member") {
                    inputControlIds = getMemberNames(dataset,
                        groupNames,
                        selectionType && selectionType === "select by group",
                        checkBoxIds);
                } else {
                    inputControlIds = groupNames;
                }

                inputControlIds.forEach(currentCheckboxId => {

                    document.getElementById(currentCheckboxId)

                        .addEventListener('click', function () {

                            // if you don't destroy the existing chart,
                            // the previous chart will reappear
                            // when you do a mouse over
                            if (chartType === configOptions.visTypePie) {

                                currentChart.destroy();
                                drawPieChart(dataset, currentCheckboxId, configOptions);

                            } else if ((chartType === configOptions.visTypeBarVertical) || (chartType === configOptions.visTypeBarHorizontal)) {

                                currentChart.destroy();
                                drawBarChart(dataset, configOptions, (chartType === configOptions.visTypeBarVertical ? "bar" : "horizontalBar"), (barChartStyle === "stacked"), (selectionType === "by group"), false, xAxisLabel, yAxisLabel);

                            }
                        });
                });

                if (chartType === configOptions.visTypePie) {
                    drawPieChart(dataset, checkBoxIds[0], configOptions);
                } else if ((chartType === configOptions.visTypeBarVertical) || (chartType === configOptions.visTypeBarHorizontal)) {
                    drawBarChart(dataset, configOptions, (chartType === configOptions.visTypeBarVertical ? "bar" : "horizontalBar"), (barChartStyle === "stacked"), (selectionType === "by group"), true, xAxisLabel, yAxisLabel);
                }

            } else if (chartType === configOptions.visTypeLine) {

                let chartTitle = yAxisLabel + " by " + xAxisLabel;
                dataset = data.datasets[0].dataset_values;
                drawLineChart(dataset, xAxisLabel, yAxisLabel, chartTitle, configOptions);

            } // else-if for graph type

        } // setUpChart


        // return determines the "public" methods of the module
        return {
            getDataset: getDataset
        }


    }
)();
