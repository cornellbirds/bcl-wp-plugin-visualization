<?php

/*
 * Plugin Name: CLO Visualization
 * Plugin URI:
 * Description: Visualization content for CLO websites
 * Version: 1.0.0
 * Author: Phil Glaser
 * Author URI:
 * License:
 */


// we don't need the template builder unless we need to make the template selectable from an ordinary page post.
// What we're doing now, instead, is associating the template directly with the Visualization post type
// include_once("class/class.visualization_template_builder.php"). In other words, the only context in which
// the


// ****************** INCLUDES
//include_once("class/class.field_editor.php");
include_once("class/class.visualization_taxonomy.php");
include_once("class/class.rest_controller.php");
include_once("class/class.field-configuration-visualization.php");
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/class.file-download.php');
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/class.field_editor.php');
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/class.post_type_config.php');


// ******************* DEFINES
define("VISUALIZATION_POST_TYPE_NAME", "visualization", true);
define("VISUALIZATION_POST_TYPE_LABEL", "Visualizations", true);
define("VISUALIZATION_POST_TYPE_TAXONOMY_NAME", "visualization_taxonomy", true);
define("VISUALIZATION_POST_TYPE_TAXONOMY_NAME_LABEL", ucwords(str_replace("_", " ", VISUALIZATION_POST_TYPE_TAXONOMY_NAME)));


define("VISUALIZATION_TYPE", "VISUALIZATION_TYPE", true);
define("X_AXIS_LABEL", "X_AXIS_LABEL", true);
define("Y_AXIS_LABEL", "Y_AXIS_LABEL", true);
define("DATA_SELECT_LABEL", "DATA_SELECT_LABEL", true);
define("SELECTION_TYPE", "SELECTION_TYPE", true);
define("BAR_STYLE", "BAR_STYLE", true);
define("FORM_FIELD_CLASS", "form-field form-required", true);

define("FLD_TXT_PIE", "Pie", true);
define("FLD_TXT_BAR_VERTICAL", "Vertical Bar", true);
define("FLD_TXT_BAR_HORIZONTAL", "Horizontal Bar", true);
define("FLD_TXT_LINE", "Line", true);


// ****************** FUNCTIONS
function associate_template($template)
{
    $post_types = array('visualization');
    if (is_singular($post_types)) {
        $template = plugin_dir_path(__FILE__) . 'tpl/tpl.visualization.php';
    }

    return $template;
}

function add_admin_js()
{

    wp_enqueue_script('admin-js',
        plugins_url('js/admin-fields.js', __FILE__), array('jquery'));

}

function sp_wp_enqueue_scripts()
{

    wp_enqueue_script('chart-js',
        plugins_url('node_modules/chart.js/dist/Chart.min.js', __FILE__));

    // this plugin will put the labels outside the pie slices and generate the percentage values automatically
    wp_enqueue_script('chart-js-piechart-outlabels',
        plugins_url('node_modules/chartjs-plugin-piechart-outlabels/dist/chartjs-plugin-piechart-outlabels.min.js', __FILE__));

    wp_enqueue_script('chart-builder',
        plugins_url('js/chart-builder.js', __FILE__), array('jquery', 'chart-js', 'chart-js-piechart-outlabels'));

    wp_register_style('chart-css',
        plugins_url('css/clo-visualization-chart.css', __FILE__), array(), '1.0', 'all');
    wp_enqueue_style('chart-css');

} // sp_wp_enqueue_scripts()

function register_visualization_post_type()
{
    if (class_exists('PostTypeConfig')) {
        $post_config_array = PostTypeConfig::make_post_type_config(VISUALIZATION_POST_TYPE_LABEL,
            array('template', // Supports Array
                'title',
                'editor',
                'custom-fields',
                'page-attributes',
                'comments',
                'discussion'));

        register_post_type(VISUALIZATION_POST_TYPE_NAME, $post_config_array);
    }
}

function initialize()
{

// initialization hooks
    add_action('init', 'VisualizationTaxonomy::register_visualization_taxonomy');
    add_action('init', 'FileDownload::download_file_hook');

    add_action('init', 'register_visualization_post_type');

// meta data related hooks
    if (class_exists('FieldEditor')) {

        FieldEditor::addFieldConfiguration(new FieldConfigurationVisualization());

        add_action('do_meta_boxes', 'FieldEditor::remove_default_fields', 10, 3);
        add_action('do_meta_boxes', 'FieldEditor::create_meta_box');
        add_action('save_post_visualization', 'FieldEditor::save_custom_fields', 1, 2);
    }

// associate template with post type
    add_filter('template_include', 'associate_template');


// javascript for manipulating custom fields on the post type admin page
    add_action('admin_enqueue_scripts', 'add_admin_js');


// javascript code for inclusion on the visualization post (as rendered by the template)
    add_action("wp_enqueue_scripts", "sp_wp_enqueue_scripts");

// add REST controller for the page's data retrieval ajax call
    add_action('rest_api_init', function () {
        $rest_controller = new RestController();
        $rest_controller->register_routes();
    });

// from https://gist.github.com/rmpel/e1e2452ca06ab621fe061e0fde7ae150
// enable csv uploads until issue is fixed in word press
    add_filter('wp_check_filetype_and_ext', function ($values, $file, $filename, $mimes) {
        if (extension_loaded('fileinfo')) {
            // with the php-extension, a CSV file is issues type text/plain so we fix that back to
            // text/csv by trusting the file extension.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $real_mime = finfo_file($finfo, $file);
            finfo_close($finfo);
            if ($real_mime === 'text/plain' && preg_match('/\.(csv)$/i', $filename)) {
                $values['ext'] = 'csv';
                $values['type'] = 'text/csv';
            }
        } else {
            // without the php-extension, we probably don't have the issue at all, but just to be sure...
            if (preg_match('/\.(csv)$/i', $filename)) {
                $values['ext'] = 'csv';
                $values['type'] = 'text/csv';
            }
        }
        return $values;
    }, PHP_INT_MAX, 4);

} // initialize()

add_action('plugins_loaded', 'initialize');

?>
