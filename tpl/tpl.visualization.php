<?php
/*
Template Name: Visualization
Template Post Type: visualization
 */

include_once(plugin_dir_path(__FILE__) . "../class/class.visualization-data.php");
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/class.file-download.php');

// *********************************************************
// ************ BUILD THE DATA WE WILL NEED FOR THE TEMPLATE
$post_errors = "";

$visualization_data = new VisualizationData();
$post_id = get_the_ID();
$attachment_id = $visualization_data->get_attachment_id($post_id);

// ----------------- set group selection control input type
$input_type = "";
$chart_type = strtolower(get_post_meta($post_id, VISUALIZATION_TYPE, true));
if ($chart_type == 'pie') {
    $input_type = 'radio';
} else if (strpos($chart_type, 'bar') !== false) {
    $input_type = 'checkbox';
}

$selection_type = strtolower(get_post_meta($post_id, SELECTION_TYPE, true));

$display_legend = (strpos($chart_type, "bar") ? 1 : 0);

$names_for_selection = Array();
if ($chart_type == "horizontal bar" || $chart_type == "vertical bar" || $chart_type == "pie") {

    if ($selection_type == "by member") {

        if ($chart_type != "pie") {

            $names_for_selection = $visualization_data->get_members_for_grouped_dataset($post_id, $post_errors);

        } else {
            $post_errors = "Select by member is not a valid option for pie charts";
        }

    } else {
        $names_for_selection = $visualization_data->get_groups_for_grouped_dataset($post_id, $post_errors);
    }

} else if ($chart_type == "line") {

    if (empty($visualization_data->get_attachment_id($post_id))) {
        $post_errors = "There is no attachment for post " . $post_id;
    }

}

$other_pages_of_taxonomy = VisualizationTaxonomy::get_pages_of_same_taxonomy_type($post_id, $post_errors);
$download_image_name = strtolower(preg_replace("/[^A-Za-z0-9s+]/", '_', get_the_title()));

// ------------------- tag attribute values that will need ot be passed into the chart builder module
$canvas_area = "drawing-area";
$group_input_name = "group_selector";
$legend_area = "bp-visualization-legend";
$download_image_element_id = "download-box-id";
?>


<!--*********************************************************-->
<!--************ NOW CONSUME THE DATA WE BUILT TO CREATE THE PAGE FROM THE TEMPLATE-->

<?php get_header(); ?>

<?php if (empty($post_errors)): ?>


    <div class="wrap content col-2 clearfix">
        <?php get_template_part('subnav'); ?>
        <article class="grid" id="bp-visualization-content" role="article">

            <h1><?php the_title(); ?></h1>

            <?php if (($chart_type == "pie") || (strpos($chart_type, 'bar') !== false)): ?>
                <div class="offset-sidebar">
                    <div class="h4"><?= join('', get_post_meta($post_id, DATA_SELECT_LABEL)) ?></div>
                    <?php for ($idx = 0; $idx < sizeof($names_for_selection); $idx++):
                        $current_group_name = $names_for_selection[$idx]; ?>
                        <div class="form-check">
                            <input class="form-check-input"
                                   type=<?= $input_type ?>
                                   <?php if (($idx == 0) || $input_type == "checkbox"): ?>checked<?php endif; ?>
                                   value="<?= $current_group_name ?>"
                                   id="<?= $current_group_name ?>"
                                   name="<?= $group_input_name ?>">

                            <label class="form-check-label" for="species_selector">
                                <?= $current_group_name ?>
                            </label>

                        </div> <!-- form check -->
                    <?php endfor; ?>
                </div> <!-- checkbox sidebar -->
            <?php endif; ?>

            <div class="offset-sidebar navigation">
                <?php if (sizeof($other_pages_of_taxonomy) > 0): ?>
                    <div class="h4">More Visualizations</div>
                    <ul>
                        <?php for ($idx = 0; $idx < sizeof($other_pages_of_taxonomy); $idx++):
                            $current_study_page = $other_pages_of_taxonomy[$idx]; ?>
                            <li>
                                <a href="<?= $current_study_page->guid ?>"><?= $current_study_page->post_title ?></a>
                            </li>
                        <?php endfor; ?>
                    </ul>
                <?php else: ?>
                    <p>There are no pages for study <?= $study_name ?> in
                        the <?= VISUALIZATION_POST_TYPE_TAXONOMY_NAME_LABEL ?></p>
                <?php endif; ?>
            </div>

            <div id="bp-visualization-container">
                <?php if ($display_legend == true): ?>
                    <div id="legend-holder">
                        <div class="h4">Legend</div>
                        <div id="<?= $legend_area ?>"></div>
                    </div>
                <?php endif; ?>

                <div id="canvas-holder">
                    <canvas height="400" width="600" id="<?= $canvas_area ?>"
                    </canvas>
                </div><!-- canvas holder -->

                <div class="bp-visualization-description">

                    <div class="bp-visualization-downloads">
                        <div class="h4">Click To Download</div>
                        <?= FileDownload::make_download_link($post_id, $attachment_id, DOWNLOAD_TYPE_ATTACHMENT, VISUALIZATION_POST_TYPE_NAME, "Data") ?>
                        <a id=<?= $download_image_element_id ?> download="<?= $download_image_name ?>">Image</a>
                    </div> <!-- visualization-downloads -->
                    
                    <?php
                        the_post();
                        echo the_content();
                        ?>

                </div> <!-- visualization-content -->

            </div><!-- bp-visualization-container -->

        </article>
    </div>

    <script>
        window.onload = function () {

            CLO_CHART_BUILDER.getDataset(null,
                null,
                {
                    datsetUrlStem: '/wp-json/clo-visualization/v1/datasets/',
                    postId: <?= $post_id ?>,
                    cavnasArea: '<?= $canvas_area ?>',
                    groupInputSelectorName: '<?= $group_input_name ?>',
                    legendArea: '<?=$legend_area ?>',
                    visTypeBarVertical: '<?=FLD_TXT_BAR_VERTICAL?>',
                    visTypeBarHorizontal: '<?=FLD_TXT_BAR_HORIZONTAL?>',
                    visTypePie: '<?=FLD_TXT_PIE?>',
                    visTypeLine: '<?=FLD_TXT_LINE?>',
                    downloadImageElementId: '<?=$download_image_element_id?>',
                    displayLegend: <?= $display_legend == 1 ? 'true' : 'false' ?>
                }
            );
        };
    </script>

<?php else: ?>

    <pre><?= $post_errors ?></pre>

<?php endif; ?>


<?php get_template_part('next-prev'); ?>
<?php //get_template_part('comment-area'); ?>
<?php if (comments_open()) { ?>
    <section id="comment-area" class="container">
        <script>
            var disqus_config = function () {
                this.page.url = '<?php get_the_permalink() ?>';
                this.page.identifier = '<?php get_the_ID() ?>';
            };
            (function () {
                var d = document, s = d.createElement('script');
                s.src = 'https://birdcams-lab.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <div id="disqus_thread"></div>
    </section>
<?php } ?>
<?php get_footer(); ?>
